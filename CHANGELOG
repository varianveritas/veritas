2015-05-04 Alan Chang <ahchang012@gmail.com>

General and user interface changes:
	* Plot xml is no longer modal, and present on the main window
	* Display + button elements rearranged to accomodate inline plotting
	* Inside treatment vs outside treatment distinction now a dropdown on main window
	* Loading up an XML now allows everything except for imaging to have access to the loaded XML
	* MV Beam on button now changes color upon loading XML beam
	* Slight tweaks to UI elements on Imaging modal
	* Continuous imaging now properly supported as a first class UI element

NEW FEATURES:
	* Imaging points now are saved automatically upon navigation
	* Now user can create a mock TrueBeam point from scratch on the imaging modal
	* Automatically ensures that a user defines both start and stop points for continuous imaging
	* User can now plot any two XML elements against each other, instead of merely MU and GantryRtn

BUGFIXES:
	* MV imaging points no longer produces buggy and invalid XML schema
	* Previous button on imaging modal now works

BACKEND CHANGES:
	* Data flow for CPData is now separated in its own model in models/CPData.py
	* UI logic moved out from utility functions and into mainWindow.py / beamon.py
	* Now lxml is used to create XML tree
	* Data is no longer transferred via temporary files, decreasing load times on creating large beams
	* Perfunctory tests now added to ensure no regressions
	* Initialization functions now have their purpose explained by smaller helper functions instead of comments
	* Navigation code in veritas/beamon.py is vastly simplified
