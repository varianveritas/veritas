# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file '.\subBeam.ui'
#
# Created: Mon Sep 09 12:01:40 2013
#      by: pyside-uic 0.2.15 running on PySide 1.2.1
#
# WARNING! All changes made in this file will be lost!

from PySide import QtCore, QtGui

class Ui_subBeam(object):
    def setupUi(self, subBeam):
        subBeam.setObjectName("subBeam")
        subBeam.resize(319, 296)
        self.buttonBox = QtGui.QDialogButtonBox(subBeam)
        self.buttonBox.setGeometry(QtCore.QRect(100, 230, 171, 32))
        self.buttonBox.setOrientation(QtCore.Qt.Horizontal)
        self.buttonBox.setStandardButtons(QtGui.QDialogButtonBox.Cancel|QtGui.QDialogButtonBox.Ok)
        self.buttonBox.setObjectName("buttonBox")
        self.Name = QtGui.QLineEdit(subBeam)
        self.Name.setGeometry(QtCore.QRect(161, 113, 91, 20))
        self.Name.setMaxLength(20)
        self.Name.setObjectName("Name")
        self.SubbeamGUID = QtGui.QLineEdit(subBeam)
        self.SubbeamGUID.setGeometry(QtCore.QRect(162, 73, 41, 20))
        self.SubbeamGUID.setObjectName("SubbeamGUID")
        self.label_13 = QtGui.QLabel(subBeam)
        self.label_13.setGeometry(QtCore.QRect(54, 147, 91, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_13.setFont(font)
        self.label_13.setObjectName("label_13")
        self.label_12 = QtGui.QLabel(subBeam)
        self.label_12.setGeometry(QtCore.QRect(101, 108, 61, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_12.setFont(font)
        self.label_12.setObjectName("label_12")
        self.MaxRadTime = QtGui.QLineEdit(subBeam)
        self.MaxRadTime.setGeometry(QtCore.QRect(161, 153, 41, 20))
        self.MaxRadTime.setObjectName("MaxRadTime")
        self.Seq = QtGui.QLineEdit(subBeam)
        self.Seq.setGeometry(QtCore.QRect(162, 37, 41, 20))
        self.Seq.setObjectName("Seq")
        self.label_11 = QtGui.QLabel(subBeam)
        self.label_11.setGeometry(QtCore.QRect(43, 70, 101, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_11.setFont(font)
        self.label_11.setObjectName("label_11")
        self.label_10 = QtGui.QLabel(subBeam)
        self.label_10.setGeometry(QtCore.QRect(115, 30, 31, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_10.setFont(font)
        self.label_10.setObjectName("label_10")
        self.label_14 = QtGui.QLabel(subBeam)
        self.label_14.setGeometry(QtCore.QRect(18, 185, 131, 31))
        font = QtGui.QFont()
        font.setPointSize(11)
        self.label_14.setFont(font)
        self.label_14.setObjectName("label_14")
        self.TrackingTrainingOnly = QtGui.QLineEdit(subBeam)
        self.TrackingTrainingOnly.setGeometry(QtCore.QRect(161, 191, 40, 20))
        self.TrackingTrainingOnly.setObjectName("TrackingTrainingOnly")

        self.retranslateUi(subBeam)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("accepted()"), subBeam.accept)
        QtCore.QObject.connect(self.buttonBox, QtCore.SIGNAL("rejected()"), subBeam.reject)
        QtCore.QMetaObject.connectSlotsByName(subBeam)

    def retranslateUi(self, subBeam):
        subBeam.setWindowTitle(QtGui.QApplication.translate("subBeam", "suBeam", None, QtGui.QApplication.UnicodeUTF8))
        self.label_13.setText(QtGui.QApplication.translate("subBeam", "MaxRadTime", None, QtGui.QApplication.UnicodeUTF8))
        self.label_12.setText(QtGui.QApplication.translate("subBeam", "Name", None, QtGui.QApplication.UnicodeUTF8))
        self.label_11.setText(QtGui.QApplication.translate("subBeam", "SubbeamGUID", None, QtGui.QApplication.UnicodeUTF8))
        self.label_10.setText(QtGui.QApplication.translate("subBeam", "Seq", None, QtGui.QApplication.UnicodeUTF8))
        self.label_14.setText(QtGui.QApplication.translate("subBeam", "TrackingTrainigOnly", None, QtGui.QApplication.UnicodeUTF8))

