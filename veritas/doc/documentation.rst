Documentation
=============

The Varian Veritas version 2.2 has three components: mainwindow, beamon and imaging.

Main window
+++++++++++

The ``mainWindow``
------------------
.. automodule:: mainWindow

   .. autoclass:: MainWindow
      :members: loadFile, openBeamonHeader, openImagingWindow, dcm2xml, plotXML


Functionalities
+++++++++++++++

The ``MV beamON`` module
------------------------
.. automodule:: beamon
   
   .. autoclass:: beamonHeader
      :members: accept, reject, openTolTable, openVelTable
   
   .. autoclass:: beamonCP
      :members: get_data, editAddCP, doneCP, openMlcFile, randomCpoint, firstCP, lastCP, nextCP, prevCP

The ``kv and MV imaging`` module
--------------------------------

.. automodule:: imaging

   .. autoclass:: cpImage
   	:members: prevCP, nextCP, randomCP, doneCP, openImageMode, selectMode

Utility Functions
+++++++++++++++++

The ``dicom2xml`` module
------------------------
.. automodule:: utils.dicom2xml

   .. autoclass:: Dicom2Xml
      :members:
       
The ``plotXML`` module
----------------------
.. automodule:: utils.plotXML

.. autofunction:: plot_fig
