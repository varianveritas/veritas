Preface
+++++++

This preface gives a high level overview of the design and implementation of
the Veritas python application.

Source code is contained in the ./veritas folder. Ignoring folders containing
documentation, and folders for storing data used by the application, the
following is the file structure.

 |  veritas
 |  ├───models
 |  ├───tests
 |  │   ├───examples
 |  │   └───test_output
 |  ├───UI
 |  ├───UI_Qt
 |  └───utils


A typical use case is that a user starts out with no data, then, using the
Beam ON module from the main window, they slowly populate a XML tree conforming
to the TrueBeam 2.0 Schema, which then gets written to the hard drive for later use or transfer.

The primary entry point of the program is in the :class:`~mainWindow.MainWindow` class, where
the main window's event loop starts up and an empty instance of the :class:`~models.CPData.CPData`
gets initialized. When the beamon button gets pressed, control flow transfers to the
:class:`~beamon.beamonHeader`. There, CPData gets filled out and passed to :class:`~beamon.beamonCP`.
When the data is all entered from the user side, it gets sent back to CPData, which then generates
the XML tree is written to the specified file location via :meth:`~utils.SetBeam20.parse()`.

Note that currently, as of 05/12/2015, imaging.py does not obey the above paradigm, and handles
all of its logic and xml creation internally. This is, indeed a code smell and will be addressed
at some point in the future

The overall structure of the program is the MVC (Model-View-Controller) Architecture, where the
primary model is :class:`~models.CPData.CPData`, the controllers are :class:`~mainWindow.MainWindow`,
:class:`imaging.cpImage` and :class:`~beamon.beamonCP`, all of which correspond to windows or dialogs.
The views are stored in the UI/ folder, in files of form ui_CONTROLLER_FILENAME.py format. They are
generated via the pyside-uic command line tool, from the corresponding UI_Qt/CONTROLLER_FILENAME.ui
file. While the ui file can be edited by hand, the Qt Designer tool found `here <http://www.qt.io/download/>`_ 
makes constructing the UI a much easier process.

The tests folder is where all the nosetests are contained. The results of the tests are sent to the
/tests/test_output directory and compared against the required entries in /tests/examples directory. 
