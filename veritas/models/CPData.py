# Copyright (c) 2014, Varian Medical Systems, Inc. (VMS)
# All rights reserved.
#
# Veritas is an open source tool for TrueBeam Developer Mode provided by
# Varian Medical Systems, Palo Alto.
# It lets users generate XML beams without assuming any prior knowledge
# of the underlying XML-schema rules.
# This version is based on the schema for TrueBeam 1.5 and 1.6.
#
# Veritas is licensed under the Varian Open Source License.
# You may obtain a copy of the License at:
#
#      http://radiotherapyresearchtools.com/license/
#
# For questions, please send us an email at: TrueBeamDeveloper@varian.com
#
# Developer Mode is intended for non-clinical use only and is NOT cleared for use on humans.
# Created on: 11:15:56 AM, May 14, 2015
# Author: Alan Huan Chang

from collections import namedtuple
from utils import SetBeam20
import lxml.etree as ET


# *************************************
# %%%

# This file contains the data container used to store
# the xml tree to-be-generated as a python class,
# It includes some utility decorators to do
# Light type-checking as well as populating
# the data structure with proper "default" values

# CPData can be used in two ways: Either by directly
# importing the root of an XML tree or by using
# a series of getters and setters.

# The first usecase would be done like so:
# cpdata = CPData()
# root = xml.etree.parse(some_xml)
# cpdata.import(root)

# Note that lxml can also be used, and both xml and lxml require installation
# via pip and imports.

# The second usecase is too long to document succinctly here, but
# the user does need to call setters on the following methods before
# consuming the data:
# set_drate, set_energy, set_MLCmodel, add_cp.

# In either usecase, invalid values will NOT be caught until the create_xml
# method is called, at which point a TypError will be thrown.

# When the data needs to be extracted, use the create_xml
# method to have CPData return the root to a valid
# xml schema.

# *************************************

ControlPoint = namedtuple("ControlPoint",
                          """Mu GantryRtn CollRtn CouchVrt CouchLat CouchLng
                        CouchRtn X1 X2 Y1 Y2 AB No""")

TolVelTable = namedtuple("TolVelTable",
                         """GantryRtn CollRtn CouchVrt CouchLat CouchLng
                        CouchRtn X1 X2 Y1 Y2""")

class MissingCPArgException(Exception):

    def __init__(self, cp_args):
        self.missing_args = set(ControlPoint._fields).difference(cp_args)

    def __str__(self):
        return "Missing the following fields for ControlPoint: {0}".format(
            self.missing_args)


def maybe(func):
    """Check to see if all the arguments for a function
    are not None. If it is, throw exception, otherwise do the function"""

    def check(*args, **kwargs):
        if None not in args:
            return func(*args, **kwargs)
        else:
            raise TypeError("Cannot be None!")
    return check


def define_struct_check(struct):
    """Decorator creator that checks to see
if the argument to a function has the identical type signature
as the namedtuple involved. If not, fill in empty entries
with None"""
    def kw_check(func):
        def check(*args, **kwargs):
            inc_args = set(kwargs.keys())
            expect_args = set(struct._fields)
            if inc_args == expect_args:
                func(*args, **kwargs)
            else:
                missing_args = expect_args.difference(inc_args)
                missing_arg_dict = {x:None for x in missing_args}
                new_kwargs = dict(kwargs, **missing_arg_dict)
                func(*args, **new_kwargs)
        return check
    return kw_check


class CPData(object):

    def __init__(self):
        self.MLCmodel_types = ("NDS80", "NDS120", "NDS120HD")
        self.cpHeader = {"TolTable": {}, "VelTable": {}, "MLCModel": None}

        self.energy_suffix_types = ('k', 'x', 'e')
        self.energy_suffix = None
        self.energy_value = None
        self.drate = None
        self.cplist = []
        self.xml = None

    def set_MLCmodel(self, model):
        if model in self.MLCmodel_types:
            self.cpHeader["MLCModel"] = model
        else:
            raise TypeError("{0} is not a predefined MLC type!".format(model))

    def get_MLCmodel(self):
        return self.cpHeader["MLCModel"]

    @maybe
    @define_struct_check(TolVelTable)
    def set_veltable(self, **kwargs):
        self.cpHeader['VelTable'] = TolVelTable(**kwargs)._asdict()

    @maybe
    @define_struct_check(TolVelTable)
    def set_toltable(self, **kwargs):
        self.cpHeader['TolTable'] = TolVelTable(**kwargs)._asdict()


    def set_energy(self, value, suffix):
        if (value or value == 0) and suffix and suffix in self.energy_suffix_types:
            self.energy_suffix = suffix
            self.energy_value = value
            return str(value) + suffix
        else:
            error_message = "Incorrect value for energy.\n"
            error_message += "value: {0} suffix: {1}".format(value, suffix)
            raise TypeError(error_message)

    def get_energy(self):
        return str(self.energy_value) + self.energy_suffix

    def get_energy_val(self):
        return str(self.energy_value)

    def get_energy_type(self):
        return self.energy_suffix

    @maybe
    def set_drate(self, drate):
        self.drate = drate
        return self.drate

    def get_drate(self):
        return self.drate

    @maybe
    @define_struct_check(ControlPoint)
    def add_cp(self, **kwargs):
        cp = ControlPoint(**kwargs)
        self.cplist.append(cp)
        return cp

    def get_cp(self, no):
        cp = self.cplist[no]._asdict()
        del cp['No']
        return cp

    def has_header(self):
        header_elements = [self.cpHeader.get('MLCModel'), self.energy_suffix, self.energy_value,
                           self.drate]
        return all([x is not None for x in header_elements]) and len(self.cplist) > 0

    @maybe
    @define_struct_check(ControlPoint)
    def set_cp(self, index, **kwargs):
        cp = ControlPoint(**kwargs)
        self.cplist[index] = cp

    def get_all_cps(self):
        cp_list = [self.get_cp(index) for index in xrange(len(self.cplist))]
        return (cp_list if cp_list else [])

    def _val_check(self, attribute):
        return attribute in self.cpHeader.keys()


    def _import_header(self, cp_tags):
        first_cp = cp_tags[0]
        for child in first_cp.getchildren():
            if child.tag == 'MLCModel':
                self.set_MLCmodel(child.text)
            elif child.tag == ('TolTable'):
                table_dict = dict()
                for table_child in child.getchildren():
                    table_dict[table_child.tag] = table_child.text
                self.set_toltable(**table_dict)
            elif child.tag == ('VelTable'):
                table_dict = dict()
                for table_child in child.getchildren():
                    table_dict[table_child.tag] = table_child.text
                self.set_veltable(**table_dict)

        # Part of the header info is in first control point
        for child in first_cp.getchildren():
            if child.tag == 'DRate':
                self.set_drate(child.text)
            elif child.tag == 'Energy':
                self.set_energy(child.text[:-1], child.text[-1])


    def _import_cp(self, cp_tags):

        curr_cp = dict()

        for cpoint in cp_tags:
            for child in cpoint.getchildren():
                if child.tag in ControlPoint._fields:
                    curr_cp[child.tag] = child.text
                elif child.tag == 'Mlc':
                    # In case Mlc tag exists, extract B and A leaves and
                    # save it in the format similar to beamON case
                    curr_cp['AB'] = (child.find("B").text, child.find("A").text)

            self.add_cp(**curr_cp)
            curr_cp = dict()

    def import_xml(self, xml):
        '''Takes some existing xml structure and
        populate the cpdata structure with it
        xml: xml_root => void'''
        self.xml = xml
        cp_tags = xml.findall('./SetBeam/ControlPoints/Cp')
        self._import_header(cp_tags)
        self._import_cp(cp_tags)

    def _add_val(self, node, name):
        ET.SubElement(node, name).text = str(self.cpHeader[name])

    def _add_tv_table(self, cur_node, table_name):
        if table_name in self.cpHeader:
            table = ET.SubElement(cur_node, table_name)
            for key, value in self.cpHeader[table_name].items():
                if value:
                    ET.SubElement(table, key).text = str(value)

    def create_xml(self):
        '''
        Converts all the control points entered by users into an XML
        file as per the SetBeam XML schema
        '''
        # Copy if there is any thing useful in the end
        root = ET.Element('VarianResearchBeam')
        root.set('SchemaVersion', '1.0')
        beam = ET.Element('SetBeam')
        root.append(beam)

        ET.SubElement(beam, 'Id').text = '1'

        if self._val_check('MLCModel'):
            self._add_val(beam, "MLCModel")

        # Here the tolTable and velTable comes in to picture
        self._add_tv_table(beam, 'TolTable')
        self._add_tv_table(beam, 'VelTable')

        # Now starts the Accs part
        ET.SubElement(beam, 'Accs')
        cpts = ET.SubElement(beam, 'ControlPoints')
        cpt = ET.SubElement(cpts, 'Cp')
        sbeam = ET.SubElement(cpt, 'SubBeam')
        ET.SubElement(sbeam, 'Seq').text = '0'
        ET.SubElement(sbeam, 'Name').text = 'Beam ON!'

        # Now the second half of header
        ET.SubElement(cpt, 'Energy').text = self.get_energy()
        ET.SubElement(cpt, 'DRate').text = self.drate

        # For the header control point
        for key, value in self.cplist[0]._asdict().items():
            if key == "AB" and value:
                b_val, a_val = value[0], value[1]
                mlc = ET.SubElement(cpt, 'Mlc')
                ET.SubElement(mlc, 'ID').text = '1'
                ET.SubElement(mlc, 'B').text = str(b_val)
                ET.SubElement(mlc, 'A').text = str(a_val)
            elif key == 'No':
                pass
            elif value:
                ET.SubElement(cpt, key).text = str(value)

        # Now the automatic part
        for control_point in self.cplist[1:]:
            cp = ET.SubElement(cpts, 'Cp')
            for key, value in control_point._asdict().items():
                if key == "AB" and value:
                    mlc = ET.SubElement(cp, 'Mlc')
                    ET.SubElement(mlc, 'ID').text = '1'
                    ET.SubElement(mlc, 'B').text = str(value[0])
                    ET.SubElement(mlc, 'A').text = str(value[1])
                elif value == 'No':
                    pass
                elif value:
                    ET.SubElement(cp, key).text = str(value)

        self.xml = root
        return root

    def valid_xml(self, root):
        SetBeam20.parse(root, 'output.xml', True)
        # Display in browser's main window
#        self.displayOutput(self.XMLfile)


def main():
    cp_data = CPData()
    cp_data.set_MLCmodel("NDS80")
    cp_data.set_toltable(GantryRtn='10.0', CollRtn='1.6', CouchVrt="-5.4",
                         CouchLat="150.0", CouchLng="-2.7", CouchRtn="0.1",
                         X1="-16.0", X2="-14.3", Y1="14.0", Y2="4.0")
    cp_data.set_veltable(GantryRtn='10.0', CollRtn='1.6', CouchVrt="-5.4",
                         CouchLat="150.0", CouchLng="-2.7", CouchRtn="0.1",
                         X1="-16.0", X2="-14.3", Y1="14.0", Y2="5.0")
    cp_data.set_energy(2, 'k')
    cp_data.set_drate("62.0")
    cp_data.add_cp(GantryRtn='10.0', CollRtn='1.6', CouchVrt="-5.4",
                   CouchLat="150.0", CouchLng="-2.7", CouchRtn="0.1",
                   X1="-16.0", X2="-14.3", Y1="14.0", Y2="4.0", Mu='0.1',
                   No="0")

    output_xml = cp_data.create_xml()
    cp_data.valid_xml(output_xml)


if __name__ == '__main__':
    main()
